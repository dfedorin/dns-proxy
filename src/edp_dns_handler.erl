-module(edp_dns_handler).
-export([handle/1]).

handle({udp, Socket, SrcAddr, SrcPort, Data}) ->
    {ok, Response} = edp_dns_proxy:proxy(Data),
    gen_udp:send(Socket, SrcAddr, SrcPort, Response).

