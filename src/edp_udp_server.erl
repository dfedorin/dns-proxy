-module(edp_udp_server).
-behaviour(gen_server).

-export([start_link/1, stop/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-record(state, {socket}).

% interface

start_link(UdpPort) ->
    gen_server:start_link(?MODULE, [UdpPort], []).

stop() ->
    gen_server:cast(?MODULE, stop).

% callbacks

init([UdpPort]) ->
    {ok, Socket} = gen_udp:open(UdpPort, [binary, {active, true}]),
    {ok, #state{socket=Socket}}.

handle_call(_Request, _From, State) ->
    {noreply, State}.

handle_cast(stop, State) ->
    {stop, normal, State}.

handle_info(UdpPkt, State) ->
    spawn(edp_dns_handler, handle, [UdpPkt]),
    {noreply, State}.

terminate(_Reason, #state{socket=Socket}) ->
    gen_udp:close(Socket),
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

