-module(edp_conf_server).
-behaviour(gen_server).

-export([start_link/1]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).

-export([get/1, load/1]).

-define(SERVER, ?MODULE).

-record(state, {conf}).

% interface

start_link(Config) ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [Config], []).

% callbacks

init(ConfigPath) ->
    {ok, Config} = read_config(ConfigPath),
    {ok, #state{conf=Config}}.

handle_call({get, Tag}, _From, State) ->
    Reply = case lists:keyfind(Tag, 1, State#state.conf) of
                {Tag, Value} ->
                    Value;
                false ->
                    {error, noinstance}
            end,
    {reply, Reply, State};
handle_call({load, ConfigPath}, _From, _State) ->
    {ok, Config} = read_config(ConfigPath),
    {reply, ok, #state{conf=Config}}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

terminate(_Reason, _State) ->
    ok.

get(Tag) ->
    gen_server:call(?MODULE, {get, Tag}).

load(ConfigPath) ->
    gen_server:call(?MODULE, {load, ConfigPath}).

% private

read_config(ConfigPath) ->
    {ok, C} = file:consult(ConfigPath),
    {Servers, Options} = lists:partition(fun(T) -> element(1, T) == dns end, C),
    {ok, [{dns_tree, create_dns_tree(Servers, [])} | Options]}.

create_dns_tree([], Conf) ->
    Conf;
create_dns_tree([{dns, U, S}|Rest], Conf) ->
    NewConf = append_to_dns_tree(Conf, lists:reverse(string:tokens(U, ".")), S),
    create_dns_tree(Rest, NewConf).

append_to_dns_tree(Conf, [], _S) ->
    Conf;
append_to_dns_tree(Conf, [D], S) ->
    case lists:keyfind(D, 2, Conf) of
        {rec, D, Srv, Chld} ->
            lists:keyreplace(D, 2, Conf, {rec, D, [S | Srv], Chld});
        false ->
            [{rec, D, S, []} | Conf]
    end;
append_to_dns_tree(Conf, [D|T], S) ->
    case lists:keyfind(D, 2, Conf) of
        {rec, D, Srv, Chld} ->
            lists:keyreplace(D, 2, Conf, {rec, D, Srv, append_to_dns_tree(Chld, T, S)});
        false ->
            [{rec, D, [], append_to_dns_tree([], T, S)} | Conf]
    end.
