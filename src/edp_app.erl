-module(edp_app).
-behaviour(application).
-export([start/2, stop/1]).

start(normal, _Args) ->
    edp_sup:start_link().

stop(_State) ->
    ok.

