-module(edp_dns_proxy).
-export([proxy/1]).

-include_lib("kernel/src/inet_dns.hrl").

proxy(DnsRequest) ->
    Servers = lookup_nameserver(DnsRequest),
    proxy(DnsRequest, Servers).

proxy(DnsRequest, Servers) ->
    {ok, Socket} = gen_udp:open(0, [binary, {active, false}]),
    Ret = proxy(DnsRequest, Socket, Servers),
    gen_udp:close(Socket),
    Ret.

proxy(_, _, []) ->
    {error, timeout};
proxy(DnsRequest, Socket, [Server|Rest]) ->
    ok = gen_udp:send(Socket, Server, 53, DnsRequest),
    case gen_udp:recv(Socket, 0, 2000) of
        {ok, {_, _, Response}} -> {ok, Response};
        {error, timeout} -> proxy(DnsRequest, Socket, Rest)
    end.

lookup_nameserver(DnsRequest) ->
    {ok, DecodedRequest} = inet_dns:decode(DnsRequest),
    [Query|_] = DecodedRequest#dns_rec.qdlist,
    lookup_nameserver(lists:reverse(string:tokens(Query#dns_query.domain, ".")),
                      edp_conf_server:get(default_dns), edp_conf_server:get(dns_tree)).

lookup_nameserver([], Servers, _) ->
    Servers;
lookup_nameserver([H|T], Servers, Record) ->
    Ret = lists:filter(fun({rec, Domain, _, _}) -> 
                           string:equal(Domain, H)
                       end, Record),
    case Ret of
        [] -> Servers;
        [{rec, _, [], NewRecord}] -> lookup_nameserver(T, Servers, NewRecord);
        [{rec, _, NewServers, NewRecord}] -> lookup_nameserver(T, NewServers, NewRecord)
    end.

