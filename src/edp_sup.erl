-module(edp_sup).
-behaviour(supervisor).
-export([start_link/0, init/1]).

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
    ConfigPath = case application:get_env(config_path) of
                     {ok, C} -> C;
                     undefined -> "/etc/edp.cfg"
                 end,
    ConfServer = {
        edp_conf_server, {edp_conf_server, start_link, [ConfigPath]},
        permanent, 2000, worker,
        [edp_conf_server]
    },
    UdpServer = {
        edp_udp_server, {edp_udp_server, start_link, [1053]},
        permanent, 2000, worker,
        [edp_udp_server]
    },
    Children = [ConfServer, UdpServer],
    RestartStrategy = {one_for_one, 3600, 4},
    {ok, {RestartStrategy, Children}}.

