{application, edp,
 [{vsn, "1.0.0"},
  {modules, [edp_app, edp_sup, edp_udp_server, edp_conf_server, edp_dns_handler, edp_dns_proxy]},
  {registered, [edp_sup]},
  {mod, {edp_app, []}},
  {env, [{config_path, "/etc/edp.cfg"}]}
 ]}.
